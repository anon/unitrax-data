# UniTraX data

UniTraX data for experiments. See main repository at https://gitlab.mpi-sws.org/anon/unitrax for more information.

This repository contains the data used in our evaluation of the UniTraX system.

The data is modified from its original state. At the time, we got the original datasets from the following web pages.

* Financial/Medical data from https://sorry.vse.cz/~berka/challenge/pkdd1999/chall.htm
* Mobility data from https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page